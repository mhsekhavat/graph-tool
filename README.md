# graph-tool conda recipe

- [Installation](#installation)
- [Enable plotting](#enable-plotting)
- [FAQ](#faq)

## Installation

[![Anaconda-Server Badge](https://anaconda.org/ostrokach-forge/graph-tool/badges/version.svg)](https://anaconda.org/ostrokach-forge/graph-tool)
[![Anaconda-Server Badge](https://anaconda.org/ostrokach-forge/graph-tool/badges/downloads.svg)](https://anaconda.org/ostrokach-forge/graph-tool)

> **Note:**
>
> This graph-tool package does not include graphing functionality!

Add required channels to your `~/.condarc`:

```bash
conda config --add channels conda-forge
conda config --add channels ostrokach-forge
```

Install graph-tool:

```bash
conda install graph-tool
```

Test your graph-tool installation:

```bash
python -c "from graph_tool.all import *"
```

## Enable plotting

> **Note:**
>
> This part does not work anymore!

In order to get graph-tool plotting to work, you also need to install:

```
conda install 'pango==1.40.0' librsvg pygobject matplotlib -c ostrokach
```

Note that this will install *a lot* of dependencies, including Glib and Xorg (~ 100 MB).

## FAQ

#### Why not submit this recipe to [conda-forge](https://conda-forge.github.io/)?

Building graph-tool requires > 4 GB memory, which is more than what is provided by TravisCI and CircleCI.
