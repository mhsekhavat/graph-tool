#!/bin/bash

# GCC 5
#yum install devtoolset-4-gcc* -y
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++

export ACLOCAL_FLAGS="-I$PREFIX/share/aclocal"
export PKG_CONFIG_PATH="$PREFIX/lib/pkgconfig:$PREFIX/share/pkgconfig"

export CFLAGS="-w -I$PREFIX/include"
export CXXFLAGS="$CFLAGS"
export CPPFLAGS="$CFLAGS"

export LDFLAGS="-L$PREFIX/lib"

export PKG_INSTALLDIR="$PREFIX"
./autogen.sh
./configure --enable-openmp --enable-static --prefix="$PREFIX" --with-boost="$PREFIX" --with-boost-libdir="$PREFIX/lib" --with-expat="$PREFIX"
make -j 8 >make_output.txt 2>&1 || tail -n 1000 make_output.txt
#make check
make install
